<?php

function console_log( $data ) {
    if (is_array($data) || is_object($data)) {
        $data = json_encode($data);
        echo "<script>console.log('PHP : ', " . $data . ")</script>";
    } else {
        echo "<script>console.log('PHP : " . $data . "')</script>";
    }
}
