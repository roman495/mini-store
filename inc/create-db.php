<?php

use \RedBeanPHP\R;

$db_host = 'localhost';
$db_name = 'payment';
$db_user = 'root';
$db_pass = '';
define('ORDERS', 'orders');

try {
    $conn = new PDO("mysql:host=$db_host", $db_user, $db_pass);    
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    console_log('Connected successfully');

    $sql = "CREATE DATABASE IF NOT EXISTS " . $db_name;
    $conn->exec($sql);
    console_log('Database created successfully');

    $sql = "USE $db_name;
    CREATE TABLE IF NOT EXISTS products (
        id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(255),
        body TEXT,
        thumbnail VARCHAR(255),
        price DECIMAL(6,2),
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    ) ENGINE = InnoDB";
    $conn->exec($sql);
    console_log('Table products created successfully');

    $sql = "USE $db_name;
    CREATE TABLE IF NOT EXISTS orders (
        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255),
        email VARCHAR(255),
        price DECIMAL(6,2),
        status ENUM('ordered', 'paid', 'delivered') DEFAULT 'ordered',
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        product_id SMALLINT UNSIGNED NOT NULL,
        CONSTRAINT `fk_orders_products`
            FOREIGN KEY (product_id) REFERENCES products (id)
            ON DELETE CASCADE
            ON UPDATE RESTRICT
    ) ENGINE = InnoDB";
    $conn->exec($sql);
    console_log('Table orders created successfully');

} catch(PDOException $e) {
    $err = htmlspecialchars($e->getMessage(), ENT_QUOTES);
    console_log('Failed: ' . $err);
}
$conn = null;

R::setup( "mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, true );
// R::freeze(true);

console_log( R::testConnection() ? 'RedBean connected successfully' : 'RedBean connected failed');

$products[] = R::load( 'products', 3 );

class Good
{
    public function __construct(
        public string $id,
        public string $title,
        public string $body,
        public string $price,
        public string $thumbnail,
    ) {} 
}

class Products implements \IteratorAggregate
{
    private array $goods = [];

    public function addGood(Good $good)
    {
        $this->goods[] = $good;
    }

    public function getGoods()
    {
        return $this->goods;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->goods);
    }
}

// $products = new Products;
// foreach (getProds() as $prod) {
//     $products->addGood(new Good(
//         id: $prod['id'],
//         title: $prod['title'],
//         body: $prod['body'],
//         price: $prod['price'],
//         thumbnail: $prod['thumbnail'],
//     ));
// }

// foreach (getProds() as $prod) {
//     $product = R::dispense( 'products' );
//     $product->title = $prod['title'];
//     $product->body = $prod['body'];
//     $product->price = $prod['price'];
//     $product->thumbnail = $prod['thumbnail'];
//     $id = R::store( $product );
// }

function getProds() {
    return [
        [
            'id'    => 1,
            'title' => 'Good 1',
            'body'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, ad.',
            'price' => 99.95,
            'thumbnail' => 'https://fakeimg.pl/300x200/'
        ],
        [
            'id'    => 2,
            'title' => 'Good 2',
            'body'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, ad.',
            'price' => 99.95,
            'thumbnail' => 'https://fakeimg.pl/300x200/'
        ],
        [
            'id'    => 3,
            'title' => 'Good 3',
            'body'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, ad.',
            'price' => 99.95,
            'thumbnail' => 'https://fakeimg.pl/300x200/'
        ],
        [
            'id'    => 4,
            'title' => 'Good 4',
            'body'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, ad.',
            'price' => 99.95,
            'thumbnail' => 'https://fakeimg.pl/300x200/'
        ],
        [
            'id'    => 5,
            'title' => 'Good 5',
            'body'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, ad.',
            'price' => 99.95,
            'thumbnail' => 'https://fakeimg.pl/300x200/'
        ],
        [
            'id'    => 6,
            'title' => 'Good 6',
            'body'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, ad.',
            'price' => 99.95,
            'thumbnail' => 'https://fakeimg.pl/300x200/'
        ],
        [
            'id'    => 7,
            'title' => 'Good 7',
            'body'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, ad.',
            'price' => 99.95,
            'thumbnail' => 'https://fakeimg.pl/300x200/'
        ],
        [
            'id'    => 8,
            'title' => 'Good 8',
            'body'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, ad.',
            'price' => 99.95,
            'thumbnail' => 'https://fakeimg.pl/300x200/'
        ],
    ];
}