<?php
require __DIR__ . '/../vendor/autoload.php';

use \RedBeanPHP\R;

$db_host = 'localhost';
$db_name = 'payment';
$db_user = 'root';
$db_pass = '';

R::setup( "mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, true );

if ($_POST) {
    $product = R::load( 'products', $_POST['product_id'] );

    $order = R::dispense( 'orders' );
    $order->name = $_POST['name'];
    $order->email = $_POST['email'];
    $order->price = $product->price;
    $order->product_id = $_POST['product_id'];
    $order_id = R::store( $order );
    
    header('Content-Type: application/json');
    echo json_encode(["id" => $order_id]);
}
