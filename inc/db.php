<?php

use \RedBeanPHP\R;

$db_host = 'localhost';
$db_name = 'payment';
$db_user = 'root';
$db_pass = '';
define('ORDERS', 'orders');

R::setup( "mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, true );

console_log( R::testConnection() ? 'RedBean connected successfully' : 'RedBean connected failed');

$products = R::findAll( 'products' );
