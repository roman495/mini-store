<?php

require_once __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/inc/helpers.php';

require_once __DIR__ . '/inc/db.php';

// require_once __DIR__ . '/inc/functions.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css">
    <title>Simple Catalog</title>
</head>
<body>
    <div class="container">
        <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
              <a class="navbar-item" href="https://bulma.io">
                <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
              </a>
          
              <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
              </a>
            </div>
          
            <div id="navbarBasicExample" class="navbar-menu">
              <div class="navbar-start">
                <a class="navbar-item">
                  Home
                </a>
          
                <a class="navbar-item">
                  Documentation
                </a>
          
                <div class="navbar-item has-dropdown is-hoverable">
                  <a class="navbar-link">
                    More
                  </a>
          
                  <div class="navbar-dropdown">
                    <a class="navbar-item">
                      About
                    </a>
                    <a class="navbar-item">
                      Jobs
                    </a>
                    <a class="navbar-item">
                      Contact
                    </a>
                    <hr class="navbar-divider">
                    <a class="navbar-item">
                      Report an issue
                    </a>
                  </div>
                </div>
              </div>
          
              <div class="navbar-end">
                <div class="navbar-item">
                  <div class="buttons">
                    <a class="button is-primary">
                      <strong>Sign up</strong>
                    </a>
                    <a class="button is-light">
                      Log in
                    </a>
                  </div>
                </div>
              </div>
            </div>
        </nav>

        <section class="section">
        <div class="columns is-multiline is-mobile">
            <?php foreach ($products as $product): ?>
            <div class="column is-one-quarter">
                <div class="card">
                    
                    <div class="card-image">
                      <figure class="image is-4by3">
                        <img src="<?= $product->thumbnail ?>" alt="Placeholder image">
                      </figure>
                    </div>

                    <div class="card-content">
                        <p class="title"><?= $product->title ?></p>
                        <div class="content">
                            <p><?= $product->body ?></p>
                            <p class="has-text-danger">Price: <?= $product->price ?></p>
                        </div>
                    </div>
                    
                    <footer class="card-footer">
                        <a 
                          href="#" 
                          data-action="buy" 
                          class="buy card-footer-item has-text-success" 
                          data-price="<?= $product->price ?>" 
                          data-product_id="<?= $product->id ?>"
                          data-product="<?= $product->title ?>"
                        >
                          Buy
                        </a>
                    </footer>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </div>
    </section>
    <footer class="footer">
        <div class="content has-text-centered">
          <p>
            <strong>Bulma</strong> by <a href="https://jgthms.com">Jeremy Thomas</a>. The source code is licensed
            <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
            is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.
          </p>
        </div>
    </footer>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="js/main.js"></script>

</body>
</html>