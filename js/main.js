const columns = document.querySelector('.columns')

columns.addEventListener('click', event => {
    event.preventDefault()
    let dataset = event.target.dataset

    if ( dataset.action === 'buy') {
        let product = dataset.product
        let product_id = dataset.product_id
        let price = dataset.price

        form = getForm( product, price, product_id )
        document.body.append(form)

        swal({
            title: 'Ordering',
            content: form,
            buttons: ['Cancel', 'Buy'],
        })
        .then(ok => {
            if (!ok) return console.log('Canseled')

            return fetch('/payment/inc/functions.php', {
                method: 'POST',
                body: new FormData( form )
            })
        })
        .then(results => {
            return results.json()
        })
        .then(json => {
            console.log(json)
            swal({
                title: 'Successfully!',
                text: 'Order ID # ' + json.id,
                icon: 'success'
            })
        }) 
    }
})

function getForm( product, price, product_id ) {
    fields = `
    <div class="has-text-left">
        <div class="field">
            <label class="label">Name</label>
            <div class="control">
              <input name="name" class="input" type="text" placeholder="Your name" required>
            </div>
        </div>
        <div class="field">
            <label class="label">Email</label>
            <div class="control">
              <input name="email" class="input" type="email" placeholder="Your email" required>
            </div>
        </div>
        <fieldset disabled="disabled">
            <div class="field">
                <label class="label">Product</label>
                <div class="control">
                  <input name="product" class="input" type="text" value="${product}">
                </div>
            </div>
            <div class="field">
                <label class="label">Price</label>
                <div class="control">
                  <input name="price" class="input" type="text" value="${price}">
                </div>
            </div>    
        </fieldset>
        <input name="product_id" type="hidden" value="${product_id}">
    </div>
    `
    let form = document.createElement('form')
    form.action = ""
    form.method = 'POST'
    form.insertAdjacentHTML('afterbegin', fields)

    return form
}
